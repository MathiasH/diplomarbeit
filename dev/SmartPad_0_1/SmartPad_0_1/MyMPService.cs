﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Media;
using System.IO;
using Android.Net.Wifi.P2p;
using static Android.Net.Wifi.P2p.WifiP2pManager;

namespace SmartPad_0_1
{
    [Service]
    class MyMPService : IntentService
    {
        public const String ActionStartMP = "SmartPad.START_MP";
        public const String ActionSendData = "SmartPad.PLAY_SOUND";
        public static String GroupOwnerAddress = "HOST_IP";

        public MyMPService()
            : base("MyMPService")
        {
        }

        public MyMPService(string name)
            : base(name)
        {
        }
        protected override void OnHandleIntent(Intent intent)
        {


            if (intent.Action.Equals(ActionStartMP))
            {
                Toast.MakeText(this, "StartMP", ToastLength.Long).Show();
                //var MPActivity = new Intent(this, typeof(MultiplayerActivity));
                //MediaPlayer player = new MediaPlayer();
                //string test = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryMusic).ToString();
                //string[] fileArr = Directory.GetFiles(test);
                //player.SetDataSource(fileArr[0]);
                //player.Start();
                
            }
        }
    }
}