﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Media;
using static Android.Widget.AdapterView;

namespace SmartPad_0_1
{
    [Activity(Label = "dataHandlingActivity")]
    public class dataHandlingActivity : Activity
    {
        MediaPlayer player;
        SoundHandler sh;
        ListView lv;
        DataBaseHelper db;
        int lastClicked = -1;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.DataHandlingLayout);

            db = new DataBaseHelper();

            List<Sound> sl = db.GetAllSounds();

            sh = new SoundHandler(sl);

            List<string> nameList = new List<string>();
            foreach (Sound s in sl)
            {
                nameList.Add(s.Name);
            }

            lv = (ListView)FindViewById(Resource.Id.soundListView);

            IListAdapter listAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleListItem1, nameList);

            lv.Adapter = listAdapter;
            ImageButton mainMenu = FindViewById<ImageButton>(Resource.Id.mainMenuBt);
            mainMenu.Click += mainMenuClick;
            ImageButton fileImport = FindViewById<ImageButton>(Resource.Id.ImportButton);
            fileImport.Click += onFileImportClick;//delegate { StartActivity(typeof(FileImportActivity)); };
            lv.ItemClick += onListItemClick;
        }

        private void onFileImportClick(object sender, EventArgs e)
        {
            try
            {
                player.Release();
            }
            catch (Exception ex)
            {
                Toast.MakeText(ApplicationContext, ex.Message, ToastLength.Long).Show();
            }
            StartActivity(typeof(FileImportActivity));
            this.Finish();
        }

        private void onListItemClick(object sender, EventArgs e)
        {

            ItemClickEventArgs iea = e as ItemClickEventArgs;
            if (iea.Position == lastClicked)
            {
                player.Stop();
                lastClicked = -1;
            }
            else
            {
                if (player == null)
                {
                    player = new MediaPlayer();
                }

                player.Reset();
                player.SetDataSource(sh.GetSoundAt(iea.Position).Path);
                lastClicked = iea.Position;
                //Don't wanna be an american idiot bababa ba baba ba baba ba ba'
                player.Prepare();
                player.Start();
            }
        }

        private void mainMenuClick(object sender, EventArgs e)
        {
            SoundButton b = sender as SoundButton;
            try
            {
                player.Release();
            }
            catch (Exception ex)
            {
                Toast.MakeText(ApplicationContext, ex.Message, ToastLength.Long).Show();
            }

            this.Finish();
        }
        private void sbOnClick(object sender, EventArgs e)
        {
            SoundButton sb = sender as SoundButton;
        }
    }
}