﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Net.Wifi.P2p;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace SmartPad_0_1
{
    interface IDeviceActionListener
    {
        void CancelDisconnect();
        void Connect(WifiP2pConfig config);
        void Disconnect();
    }
}