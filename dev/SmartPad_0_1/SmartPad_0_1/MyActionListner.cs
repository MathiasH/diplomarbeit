﻿using System;
using Android.Content;
using Android.Net.Wifi.P2p;
using Android.Widget;

namespace SmartPad_0_1
{
    public partial class LobbyActivity
    {
        private class MyActionListner : Java.Lang.Object, WifiP2pManager.IActionListener
        {
            private readonly Context context;
            private readonly string failure;
            private readonly Action action;

            public MyActionListner(Context context, string failure, Action onSuccessAction)
            {
                this.context = context;
                this.failure = failure;
                this.action = onSuccessAction;
            }

            public void OnFailure(WifiP2pFailureReason reason)
            {
                Toast.MakeText(context, failure + " Failed : " + reason,
                                ToastLength.Short).Show();
            }

            public void OnSuccess()
            {
                Toast.MakeText(context, failure + " Initiated",
                                ToastLength.Short).Show();
                action.Invoke();
            }
        }
    }
}