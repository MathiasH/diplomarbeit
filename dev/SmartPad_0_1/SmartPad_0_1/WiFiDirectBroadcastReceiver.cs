﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Net;
using Android.Net.Wifi.P2p;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace SmartPad_0_1
{
    public class WiFiDirectBroadcastReceiver : BroadcastReceiver
    {

        private readonly WifiP2pManager manager;
        private readonly WifiP2pManager.Channel channel;
        private readonly LobbyActivity activity;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="manager">WifiP2pManager system service</param>
        /// <param name="channel">Wifi p2p channel</param>
        /// <param name="activity">activity associated with the receiver</param>
        public WiFiDirectBroadcastReceiver(WifiP2pManager manager, WifiP2pManager.Channel channel,
                                           LobbyActivity activity)
        {
            this.manager = manager;
            this.channel = channel;
            this.activity = activity;
        }

        public override void OnReceive(Context context, Intent intent)
        {
            var action = intent.Action;

            if (WifiP2pManager.WifiP2pStateChangedAction.Equals(action))
            {
                // UI update to indicate wifi p2p status.
                var state = intent.GetIntExtra(WifiP2pManager.ExtraWifiState, -1);
                if (state == (int)WifiP2pState.Enabled)
                    // Wifi Direct mode is enabled
                    activity.IsWifiP2PEnabled = true;
                else
                {
                    activity.IsWifiP2PEnabled = false;
                    activity.ResetData();
                }
                Log.Debug(LobbyActivity.Tag, "P2P state changed - " + state);
            }
            else if (WifiP2pManager.WifiP2pPeersChangedAction.Equals(action))
            {
                // request available peers from the wifi p2p manager. This is an
                // asynchronous call and the calling activity is notified with a
                // callback on PeerListListener.onPeersAvailable()
                if (manager != null)
                {
                    manager.RequestPeers(channel, activity);
                }
                Log.Debug(LobbyActivity.Tag, "P2P peers changed");
            }
            else if (WifiP2pManager.WifiP2pConnectionChangedAction.Equals(action))
            {
                if (manager == null)
                    return;

                var networkInfo = (NetworkInfo)intent.GetParcelableExtra(WifiP2pManager.ExtraNetworkInfo);

                if (networkInfo.IsConnected)
                {
                    // we are connected with the other device, request connection
                    // info to find group owner IP

                    //var fragment = activity.FragmentManager.FindFragmentById<DeviceDetailFragment>(Resource.Id.frag_detail);

                    manager.RequestConnectionInfo(channel, activity);
                }
                else
                {
                    // It's a disconnect
                    activity.ResetData();
                }
            }
            else if (WifiP2pManager.WifiP2pThisDeviceChangedAction.Equals(action))
            {
                activity.UpdateThisDevice((WifiP2pDevice)intent.GetParcelableExtra(WifiP2pManager.ExtraWifiP2pDevice));
            }
        }
    }
}