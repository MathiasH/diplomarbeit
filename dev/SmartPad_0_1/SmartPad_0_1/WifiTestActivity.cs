﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Net.Wifi.P2p;

namespace SmartPad_0_1
{
    [Activity(Label = "WifiTestActivity")]
    public class WifiTestActivity : Activity
    {
        bool retrychannel;

        IntentFilter intentFilter = new IntentFilter();
        WifiP2pManager manager;
        WifiP2pManager.Channel channel;
        BroadcastReceiver receiver;

        public bool IsWifiP2PEnabled { get; set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.WifiTestLayout);

            intentFilter.AddAction(WifiP2pManager.WifiP2pStateChangedAction);
            intentFilter.AddAction(WifiP2pManager.WifiP2pPeersChangedAction);
            intentFilter.AddAction(WifiP2pManager.WifiP2pConnectionChangedAction);
            intentFilter.AddAction(WifiP2pManager.WifiP2pThisDeviceChangedAction);

            manager = (WifiP2pManager) GetSystemService(WifiP2pService);
            channel = manager.Initialize(this, MainLooper, null);

            Button discoverButtton = FindViewById<Button>(Resource.Id.FindButton);
            discoverButtton.Click += discoverButttonClick;
        }

        private void discoverButttonClick(object sender, EventArgs e)
        {
            List<WifiP2pDeviceList> deviceList;
            manager.DiscoverPeers(channel, new MyActionListener(this, "Discovery", () => { }));

        }
    }
}

    
            