﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace SmartPad_0_1
{
    class BoardConf
    {
        [PrimaryKey, Unique]
        public int ID { get; set; }

        public int Rot { get; set; }

        public int Gruen { get; set; }

        public int Blau { get; set; }
        
        public int SoundID { get; set; }

    }
}