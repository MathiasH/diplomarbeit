﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.IO;
using Android.Media;

namespace SmartPad_0_1
{
    class SoundHandler
    {
        List<Sound> SoundList;
        public SoundHandler(List<Sound> SoundList)
        {
            this.SoundList = SoundList;
        }
        public List<Sound> GetSoundList()
        {
            return SoundList;
        }
        public Sound GetSoundAt(int pos)
        {
            return SoundList[pos];
        }
    }
}