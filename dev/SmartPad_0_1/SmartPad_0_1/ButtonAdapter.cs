using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace SmartPad_0_1
{
    class ButtonAdapter : BaseAdapter
    {
        private readonly Context context;

        public IList<string> Items { get; set; }

        public ButtonAdapter(Context context)
        {
            this.context = context;
        }
        public override Java.Lang.Object GetItem (int position)
        {
            return null;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                var layoutInflater = LayoutInflater.From(context);
                convertView = layoutInflater.Inflate(Resource.Layout.grid_item, parent);
            }
            var button = convertView.FindViewById<Button>(Resource.Id.button);
            button.Text = Items[position];

            return convertView;
        }
        public override int Count
        {
            get
            {
                return Items.Count;
            }
        }
    }
}