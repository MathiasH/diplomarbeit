﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Media;
using Android.Content.Res;
using Android.Graphics;
 

namespace SmartPad_0_1
{

    [Activity(Label = "SmartPad_0_1", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Main);

            DataBaseHelper db = new DataBaseHelper();
            
            Toast.MakeText(ApplicationContext, db.createDatabase(), ToastLength.Long).Show();
            //Button Events für jeweilige Activity hinzufügen
            Button spButton = FindViewById<Button>(Resource.Id.SinglePlayerBt);
            spButton.Click += delegate { StartActivity(typeof(SinglePlayerActivity)); };

            Button LobbyButton = FindViewById<Button>(Resource.Id.lobbyBt);
            LobbyButton.Click += delegate { StartActivity(typeof(LobbyActivity)); };

            Button settingsButton = FindViewById<Button>(Resource.Id.settingsBt);
            settingsButton.Click += delegate { StartActivity(typeof(AufnahmetestActivity)); };

            Button importButton = FindViewById<Button>(Resource.Id.importBt);
            importButton.Click += delegate { StartActivity(typeof(dataHandlingActivity)); };
        }
        
    }
}

