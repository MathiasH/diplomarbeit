using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Media;
using Android.Util;

namespace SmartPad_0_1
{
    
    class SoundButton : Button
    {
        public Sound sound;
        bool repeat;
        public int sId;

        public SoundButton(Context context, bool repeat, Sound sound,int sId) : base(context)
        {
            this.sId = sId;
            this.sound = sound;
            this.repeat = repeat;
            
        }
        public SoundButton(Context context, bool repeat, int sId) : base(context)
        {
            this.sId = sId;
            this.repeat = repeat;
        }
    }
}