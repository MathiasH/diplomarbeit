﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

using Android.App;
using Android.Content;
using Android.Net.Wifi;
using Android.Net.Wifi.P2p;
using Android.Net.Wifi.P2p.Nsd;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using static Android.Widget.AdapterView;

namespace SmartPad_0_1
{
    [Activity(Label = "LobbyActivity")]
    [IntentFilter(new[] { "SmartPad.START_MP"})]
    public partial class LobbyActivity : Activity, WifiP2pManager.IChannelListener, WifiP2pManager.IPeerListListener, WifiP2pManager.IConnectionInfoListener, IDeviceActionListener
    {
        public const string Tag = "lobby";
        private WifiP2pManager manager;
        private bool retryChannel;
        

        private readonly List<WifiP2pDevice> peers = new List<WifiP2pDevice>();
        private readonly IntentFilter intentFilter = new IntentFilter();
        private WifiP2pManager.Channel channel;
        private WifiP2pDevice device;
        private WifiP2pInfo info;
        private BroadcastReceiver receiver;
        private WifiP2pServiceInfo service;

        public const int SERVER_PORT = 8888;

        ListView lv;

        ProgressBar progressBar;

        private Dictionary<string, string> buddies = new Dictionary<string, string>();
        public bool IsWifiP2PEnabled { get; set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ConnectionTestLayout);

            intentFilter.AddAction(WifiP2pManager.WifiP2pStateChangedAction);
            intentFilter.AddAction(WifiP2pManager.WifiP2pPeersChangedAction);
            intentFilter.AddAction(WifiP2pManager.WifiP2pConnectionChangedAction);
            intentFilter.AddAction(WifiP2pManager.WifiP2pThisDeviceChangedAction);

            //Progress Bar
            progressBar = new ProgressBar(this, null, Android.Resource.Attribute.ProgressBarStyleHorizontal);
            LinearLayout layout = FindViewById<LinearLayout>(Resource.Id.ConnectionLayout);
            layout.AddView(progressBar, 0);
            progressBar.Indeterminate = true;
            progressBar.Visibility = ViewStates.Gone;

            Button buttonDisconnect = FindViewById<Button>(Resource.Id.buttonDisconnect);
            buttonDisconnect.Visibility = ViewStates.Gone;
            buttonDisconnect.Click += onDisconnectButtonClick;

            Button buttonStartMP = FindViewById<Button>(Resource.Id.buttonStartMP);
            buttonStartMP.Visibility = ViewStates.Gone;
            buttonStartMP.Click += onStartMPButtonClick;

            Button buttonStartStream = FindViewById<Button>(Resource.Id.buttonStartStream);
            buttonStartStream.Visibility = ViewStates.Gone;
            buttonStartStream.Click += onStartStreamButtonClick;

            lv = FindViewById<ListView>(Resource.Id.listViewHost);
            lv.ItemClick += OnListViewItemClick;

            List<string> nameList = new List<string>();
            foreach (var peer in peers)
            {
                nameList.Add(peer.DeviceName);
            }
            IListAdapter listAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, nameList);

            manager = (WifiP2pManager)GetSystemService(WifiP2pService);
            channel = manager.Initialize(this, MainLooper, null);

            Button discoverButton = FindViewById<Button>(Resource.Id.buttonDiscover);
            discoverButton.Click += onDiscoverButtonClick;
              
            
            
        }

        private void onStartStreamButtonClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void onStartMPButtonClick(object sender, EventArgs e)
        {
            Intent startMP = new Intent(this, typeof(MyMPService));
            startMP.SetAction(MyMPService.ActionStartMP);

            this.StartService(startMP);

            //var multiplayerActivity = new Intent(this, typeof(MultiplayerActivity));
            //var StartMPIntent = new Intent();
            //StartMPIntent.SetAction(Intent.ActionSend);
            //StartMPIntent.PutExtra("action", "startMP");
            //this.StartActivity(StartMPIntent);
        }

        public void UpdateThisDevice(WifiP2pDevice wifiP2pDevice)
        {
            this.device = wifiP2pDevice;
            Toast.MakeText(this, "Connected to: " + wifiP2pDevice.DeviceName, ToastLength.Short);
        }

        private void onDisconnectButtonClick(object sender, EventArgs e)
        {
            Disconnect();
        }

        private void OnListViewItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            LayoutInflater layoutInflater = LayoutInflater.From(this);
            View view = layoutInflater.Inflate(Resource.Layout.connectionDialog, null);
            AlertDialog.Builder alertbuilder = new AlertDialog.Builder(this);
            
            alertbuilder.SetView(view);
            alertbuilder.SetCancelable(false)
                .SetPositiveButton("Connect", ConnectButtonClick
                //delegate{Toast.MakeText(this, "Starte Verbindung", ToastLength.Short).Show();}
                )
                .SetNegativeButton("Cancel", delegate
                {
                    alertbuilder.Dispose();
                })
                .SetTitle(peers[e.Position].DeviceName);
            device = peers[e.Position];
            AlertDialog alertDialog = alertbuilder.Create();
            alertDialog.Show();
        }

        private void ConnectButtonClick(object sender, DialogClickEventArgs e)
        {
            var config = new WifiP2pConfig
            {
                DeviceAddress = device.DeviceAddress,
                Wps =
                {
                    Setup = WpsInfo.Pbc
                }
            };

            CheckBox checkBox = FindViewById<CheckBox>(Resource.Id.checkBoxHost);
            if (checkBox.Checked == true)
            {
                config.GroupOwnerIntent = 15;
            }
            else
            {
                config.GroupOwnerIntent = 0;
            }
            
            if (progressBar != null && progressBar.Visibility == ViewStates.Visible)
                progressBar.Visibility = ViewStates.Gone;

            progressBar.Visibility = ViewStates.Visible;
            Toast.MakeText(this, "Connecting to: " + device.DeviceAddress, ToastLength.Short).Show();

            ((IDeviceActionListener)this).Connect(config);
        }

        internal void ResetData()
        {
            
            if (lv != null)
            {
                peers.Clear();
            }
        }

        private void onDiscoverButtonClick(object sender, EventArgs e)
        {
            onInitiateDiscovery();
            manager.DiscoverPeers(channel, new MyActionListner(this, "Discovery", () => { }));
        }

        private void onInitiateDiscovery()
        {
            if (progressBar != null && progressBar.Visibility == ViewStates.Visible)
                progressBar.Visibility = ViewStates.Gone;
            progressBar.Visibility = ViewStates.Visible;
        }

        public void OnPeersAvailable(WifiP2pDeviceList peers)
        {

            if (progressBar != null && progressBar.Visibility == ViewStates.Visible)
                progressBar.Visibility = ViewStates.Gone;
            this.peers.Clear();
            List<string> nameList = new List<string>();
            ListView lv = FindViewById<ListView>(Resource.Id.listViewHost);
            foreach (var peer in peers.DeviceList)
            {
                this.peers.Add(peer);
            }
            foreach (var peer in this.peers)
            {
                nameList.Add(peer.DeviceName);
            }
            IListAdapter listAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, nameList);
            lv.Adapter = listAdapter;

        }

        protected override void OnResume()
        {
            base.OnResume();
            receiver = new WiFiDirectBroadcastReceiver(manager, channel, this);
            RegisterReceiver(receiver, intentFilter);
        }
        protected override void OnPause()
        {
            base.OnPause();
            UnregisterReceiver(receiver);
        }

        private void startRegistration()
        {
            Dictionary<string, string> record = new Dictionary<string, string>();
            Random rand = new Random();
            record.Add("listernport", SERVER_PORT.ToString());
            record.Add("peerName", "peer" + CalculateMD5Hash(Build.GetSerial()));
             
            record.Add("available", "visible");

            WifiP2pDnsSdServiceInfo serviceInfo = WifiP2pDnsSdServiceInfo.NewInstance("test", "action.tcp", record);

            manager.AddLocalService(channel, serviceInfo, new MyActionListner(this, "ServiceRegistration", () => { }));
        }
        private void discoverService()
        {
            WifiP2pManager.IDnsSdTxtRecordListener txtRecordListener = new DnsSdTxtRecordListener();
            //txtRecordListener.OnDnsSdTxtRecordAvailable();
            
        }
        public void OnChannelDisconnected()
        {
            // we will try once more
            if (manager != null && !retryChannel)
            {
                Toast.MakeText(this, "Channel lost. Trying again", ToastLength.Long).Show();
                ResetData();
                retryChannel = true;
                manager.Initialize(this, MainLooper, this);
            }
            else
            {
                Toast.MakeText(this, "Severe! Channel is probably lost permanently. Try Disable/Re-Enable P2P.",
                               ToastLength.Long).Show();
            }
        }

        public void OnConnectionInfoAvailable(WifiP2pInfo info)
        {
            if (progressBar != null && progressBar.Visibility == ViewStates.Visible)
                progressBar.Visibility = ViewStates.Gone;

            this.info = info;

            Toast.MakeText(this, (info.IsGroupOwner) ? "You are the Host. Your IP: " + info.GroupOwnerAddress.HostAddress : "You are the Client. Host IP: "+ info.GroupOwnerAddress.HostAddress , ToastLength.Short).Show();

            if (info.GroupFormed && info.IsGroupOwner)
            {
                Button bt = FindViewById<Button>(Resource.Id.buttonStartMP);
                bt.Visibility = ViewStates.Visible;
                LinearLayout layout = FindViewById<LinearLayout>(Resource.Id.discoverLayout);
                layout.Visibility = ViewStates.Gone;
                bt = FindViewById<Button>(Resource.Id.buttonDisconnect);
                bt.Visibility = ViewStates.Visible;
            }
            else if (info.GroupFormed)
            {
                LinearLayout layout = FindViewById<LinearLayout>(Resource.Id.discoverLayout);
                layout.Visibility = ViewStates.Gone;
                Button bt = FindViewById<Button>(Resource.Id.buttonDisconnect);
                bt.Visibility = ViewStates.Visible;
            }
            

        }


        public void CancelDisconnect()
        {
            if(manager != null)
            {
                
            }
        }

        public void Connect(WifiP2pConfig config)
        {
            manager.Connect(channel, config, new MyActionListner(this, "Connect", () => { }));
        }

        public void Disconnect()
        {
            if (progressBar != null && progressBar.Visibility == ViewStates.Visible)
                progressBar.Visibility = ViewStates.Gone;

            
            manager.RemoveGroup(channel, new MyActionListner(this, "Disconnect", () =>
            {
                Button bt = FindViewById<Button>(Resource.Id.buttonStartMP);
                bt.Visibility = ViewStates.Gone;
                LinearLayout layout = FindViewById<LinearLayout>(Resource.Id.discoverLayout);
                layout.Visibility = ViewStates.Visible;
                bt = FindViewById<Button>(Resource.Id.buttonDisconnect);
                bt.Visibility = ViewStates.Gone;
            }));
        }
        public string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}