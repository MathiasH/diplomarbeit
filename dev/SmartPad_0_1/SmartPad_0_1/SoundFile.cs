﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace SmartPad_0_1
{
    class SoundFile
    {
        public string name;
        public string path;
        public int id;

        public SoundFile(string name, string path)
        {
            this.name = name;
            this.path = path;
            this.id++;
        }
        public SoundFile(string name)
        {
            this.name = name;
            id++;
        }
    }
}