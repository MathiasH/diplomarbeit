﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Media;
using System.IO;

namespace SmartPad_0_1
{
    class SoundFileHandler
    {
        List<SoundFile> SoundFileList;
        MediaMetadataRetriever reader;
        DataBaseHelper dbHelper;
        public SoundFileHandler()
        {
            dbHelper = new DataBaseHelper();
            SoundFileList = new List<SoundFile>();
            string path = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryMusic).ToString();
            string[] fileArr = Directory.GetFiles(path);
            reader = new MediaMetadataRetriever();
            foreach (string s in fileArr)
            {
                reader.SetDataSource(s);
                string name = reader.ExtractMetadata(MetadataKey.Title);
                if (name == null)
                {

                    SoundFileList.Add(new SoundFile(s, s));
                }
                else
                {
                    SoundFileList.Add(new SoundFile(name, s));
                }

            }
            var fields = typeof(Resource.Raw).GetFields();

        }
        public List<SoundFile> GetSoundFileList()
        {
            return SoundFileList;
        }
        public SoundFile GetSoundFileAt(int pos)
        {
            return SoundFileList[pos];
        }
    }
}