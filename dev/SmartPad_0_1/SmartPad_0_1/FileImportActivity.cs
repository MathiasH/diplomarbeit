﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Media;
using static Android.Widget.AdapterView;

namespace SmartPad_0_1
{
    [Activity(Label = "FileImportActivity")]
    public class FileImportActivity : Activity
    {
        MediaPlayer player;
        SoundFileHandler sh;
        ListView lv;
        DataBaseHelper db;
        int lastClicked = -1;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.FileImportLayout);
            db = new DataBaseHelper();
            sh = new SoundFileHandler();
            List<string> nameList = new List<string>();
            foreach (SoundFile s in sh.GetSoundFileList())
            {
                nameList.Add(s.name);
            }

            lv = (ListView)FindViewById(Resource.Id.soundListView);

            IListAdapter listAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleListItem1, nameList);

            lv.Adapter = listAdapter;
            ImageButton mainMenu = FindViewById<ImageButton>(Resource.Id.mainMenuBt);
            mainMenu.Click += mainMenuClick;

            ImageButton clearTable = FindViewById<ImageButton>(Resource.Id.ImportButton);
            clearTable.Click += clearTableClick;

            lv.ItemClick += onListItemClick;
            lv.ItemLongClick += onListItemLongClick;

        }
        private void onListItemClick(object sender, EventArgs e)
        {

            ItemClickEventArgs iea = e as ItemClickEventArgs;
            if (iea.Position == lastClicked)
            {
                player.Stop();
                lastClicked = -1;
            }
            else
            {
                if (player == null)
                {
                    player = new MediaPlayer();
                }

                player.Reset();
                player.SetDataSource(sh.GetSoundFileAt(iea.Position).path);
                lastClicked = iea.Position;
                //Don't wanna be an american idiot bababa ba baba ba baba ba ba
                player.Prepare();
                player.Start();
            }
        }
        private void onListItemLongClick(object sender, EventArgs e)
        {
            ItemLongClickEventArgs iea = e as ItemLongClickEventArgs;
            SoundFile sf = sh.GetSoundFileAt(iea.Position);
            Sound s = new Sound { Name = sf.name, Path = sf.path };

            string message = db.InsertUptdateSoundData(s);

            Toast.MakeText(ApplicationContext, message, ToastLength.Long).Show();
        }
        private void mainMenuClick(object sender, EventArgs e)
        {
            SoundButton b = sender as SoundButton;
            try
            {
                player.Release();
            }
            catch (Exception ex)
            {
                Toast.MakeText(ApplicationContext, ex.Message, ToastLength.Long).Show();
            }
            StartActivity(typeof(dataHandlingActivity));
            this.Finish();
        }
        private void sbOnClick(object sender, EventArgs e)
        {
            SoundButton sb = sender as SoundButton;
        }

        private void clearTableClick(object sender, EventArgs e)
        {
            string message = db.ClearSoundTable();

            Toast.MakeText(ApplicationContext, message, ToastLength.Long).Show();
        }
    }
}