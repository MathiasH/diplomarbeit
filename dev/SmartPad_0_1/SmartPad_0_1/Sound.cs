using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Media;
using SQLite;

namespace SmartPad_0_1
{
    class Sound
    {
        [PrimaryKey, AutoIncrement, Unique]
        public int ID { get; set; }

        public string Name { get; set; }
        
        public string Path { get; set; }
        
    }
}