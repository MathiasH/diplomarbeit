﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
using System.IO;

namespace SmartPad_0_1
{
    class DataBaseHelper
    {
        static string docsFolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
        private string path = System.IO.Path.Combine(docsFolder, "SmartPad.db");

        public DataBaseHelper(){}

        public string createDatabase()
        {
            try
            {
                //File.Delete(path);
                if (!File.Exists(path))
                {
                    var connection = new SQLiteConnection(path);
                    {
                        connection.CreateTable<Sound>();

                        connection.CreateTable<BoardConf>();
                        DefaultBoardConf();
                        
                        return "Database created";
                    }
                }
                else
                {
                    return "Database allready exists";
                }
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }

        private void DefaultBoardConf()
        {
            for (int i = 0; i < 25; i++)
            {

            }
        }

        public string ClearSoundTable()
        {
            try
            {
                var db = new SQLiteConnection(path);
                db.DeleteAll<Sound>();
                return ("Sound table cleared");
            }
            catch (Exception)
            {

                throw;
            }
        }
        public string InsertUpdateBoardConfData(BoardConf data)
        {
            try
            {
                var db = new SQLiteConnection(path);
                if (db.Insert(data) != 0)
                {
                    db.Update(data);
                }

                return "updated";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }
        public string UpdateBoardConfData(BoardConf data)
        {
            try
            {
                var db = new SQLiteConnection(path);
                db.Update(data);

                return "updated";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }
        public string InsertUptdateSoundData(Sound data)
        {
            try
            {
                var db = new SQLiteConnection(path);
                if (db.Insert(data) != 0)
                {
                    db.Update(data);
                }

                return "updated";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }
        public string insertUpdateAllSoundData(IEnumerable<Sound> data)
        {
            try
            {
                var db = new SQLiteConnection(path);
                if(db.InsertAll(data) != 0)
                {
                    db.UpdateAll(data);
                }
                return "Table Updated";
            }
            catch(SQLiteException ex)
            {
                return ex.Message;
            }
        }

        public List<Sound> GetAllSounds()
        {
            var db = new SQLiteConnection(path);
            List<Sound> sl = db.Query<Sound>("SELECT * FROM Sound;");
            return sl;

        }
        public List<Sound> GetSoundID(int ID)
        {
            var db = new SQLiteConnection(path);
            List<Sound> sl = db.Query<Sound>("SELECT * FROM SOUND WHERE ID=" + ID +";");
            return sl;
        }
        public List<BoardConf> GetAllBoardConf()
        {
            var db = new SQLiteConnection(path);
            List<BoardConf> bcl = db.Query<BoardConf>("SELECT * FROM BOARDCONF;");
            return bcl;
        }
        public List<BoardConf> GetBoardConfID(int ID)
        {
            var db = new SQLiteConnection(path);
            List<BoardConf> bcl = db.Query<BoardConf>("SELECT * FROM BOARDCONF WHERE ID=" + ID + ";");
            return bcl;
        }

    }
}