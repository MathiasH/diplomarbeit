﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Media;


namespace SmartPad_0_1
{
    [Activity(Label = "AufnahmetestActivity")]
    public class AufnahmetestActivity : Activity
    {
        MediaRecorder recorder;
        MediaPlayer player;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
          
            SetContentView(Resource.Layout.AufnahmeTestLayout);
            ImageButton mainMenu = FindViewById<ImageButton>(Resource.Id.mainMenuBt);
            mainMenu.Click += mainMenuClick;
            Button bt1 = (Button)this.FindViewById(Resource.Id.RecTestBt1);
            bt1.Click += bt1Click;
            Button bt2 = (Button)this.FindViewById(Resource.Id.RecTestBt2);
            bt2.Click += bt2Click;
            Button bt3 = (Button)this.FindViewById(Resource.Id.RecTestBt3);
            bt3.Click += bt3Click;
            ImageButton recordButton = (ImageButton)this.FindViewById(Resource.Id.RecordButton);
            recordButton.Click += recordButtonClick;
            // Create your application here
        }

        private void recordButtonClick(object sender, EventArgs e)
        {
            if (recorder == null)
            {
                try
                {
                    recorder = new MediaRecorder();
                    recorder.SetAudioSource(AudioSource.RemoteSubmix);
                    recorder.SetOutputFormat(OutputFormat.ThreeGpp);
                    recorder.SetAudioEncoder(AudioEncoder.Default);
                    recorder.SetOutputFile("sdcard/test.3gpp");//(Android.OS.Environment.DirectoryPodcasts).ToString() + "/test.3gpp");
                    recorder.Prepare();
                    recorder.Start();
                }
                catch (Exception ex)
                {

                    Toast.MakeText(ApplicationContext, ex.Message, ToastLength.Long).Show();
                }
                
            }
            else
            {
                recorder.Stop();
                recorder.Reset();
                recorder = null;
            }
            
        }

        private void bt1Click(object sender, EventArgs e)
        {
            var fields = typeof(Resource.Raw).GetFields();
            string s = fields[5].Name;
            int fileid = (int)typeof(Resource.Raw).GetField(s).GetValue(null);
            player = MediaPlayer.Create(this, fileid);
            player.Start();
            player.Completion += delegate { player.Release(); };
        }
        private void bt2Click(object sender, EventArgs e)
        {
            var fields = typeof(Resource.Raw).GetFields();
            string s = fields[1].Name;
            int fileid = (int)typeof(Resource.Raw).GetField(s).GetValue(null);
            player = MediaPlayer.Create(this, fileid);
            player.Start();
            player.Completion += delegate { player.Release(); };

        }
        private void bt3Click(object sender, EventArgs e)
        {
            var fields = typeof(Resource.Raw).GetFields();
            string s = fields[2].Name;
            int fileid = (int)typeof(Resource.Raw).GetField(s).GetValue(null);
            player = MediaPlayer.Create(this, fileid);
            player.Start();
            player.Completion += delegate { player.Release(); };

        }
        private void mainMenuClick(object sender, EventArgs e)
        {
            
            try
            {
                player.Release();
            }
            catch (Exception)
            {


            }

            this.Finish();
        }
    }
}