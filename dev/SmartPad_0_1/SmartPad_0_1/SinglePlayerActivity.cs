using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Media;
using Android.Content.Res;
using Android.Graphics;
using SQLite;

namespace SmartPad_0_1
{
    [Activity(Label = "SinglePlayerActivity")]
    public class SinglePlayerActivity : Activity
    {
        MediaPlayer player;
        DataBaseHelper db;
        List<MediaPlayer> mpList;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            db = new DataBaseHelper();
            mpList = new List<MediaPlayer>();
            SetContentView(Resource.Layout.singlePlayerTable);
            //GridLayout gl = (GridLayout)this.FindViewById(Resource.Id.gridLayout);
            TableLayout tl = (TableLayout)this.FindViewById(Resource.Id.tableLayout);
            List<BoardConf> bcl = db.GetAllBoardConf();
            //ListRaw();
            //TableLayout.LayoutParams layoutParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.FillParent, TableLayout.LayoutParams.FillParent);
            TableRow tr;
            TableLayout.LayoutParams trlayout = new TableLayout.LayoutParams(TableLayout.LayoutParams.MatchParent, TableLayout.LayoutParams.MatchParent);
            trlayout.Gravity = GravityFlags.Center;
            trlayout.Weight = 1;
            int btIdCount = 0;
            if (bcl.Count == 0)
            {
                while (btIdCount < 24)
                {
                    BoardConf bc = new BoardConf();
                    bc.ID = btIdCount;
                    bc.Rot = 153;
                    bc.Gruen = 153;
                    bc.Blau = 153;
                    bc.SoundID = -1;
                    db.InsertUpdateBoardConfData(bc);
                    btIdCount++;
                }
                btIdCount = 0;
                bcl = db.GetAllBoardConf();
            }
            for (int i = 0; i < 6; i++)
            {
                tr = new TableRow(this);
                //var tr = LayoutInflater.Inflate(Resource.Layout.row, tl, true);
                tr.LayoutParameters = trlayout;
                
                
                
                tl.AddView(tr);
                //tr.LayoutParameters = layoutParams;
                for (int j = 0; j < 4; j++)
                {
                    BoardConf boardConf = bcl[btIdCount];
                    SoundButton but;
                    if (boardConf.SoundID == -1)
                    {
                        but = new SoundButton(this, false, btIdCount);
                    }
                    else
                    {
                        Sound s = db.GetSoundID(boardConf.SoundID)[0];
                        but = new SoundButton(this, false, s, btIdCount);
                    }
                    

                    //SoundButton but = new SoundButton(this, false, "test", 1);

                    but.SetBackgroundColor(Color.Rgb(boardConf.Rot, boardConf.Gruen, boardConf.Blau));

                    btIdCount++;
                    //sb.SetWidth(Resources.DisplayMetrics.WidthPixels/4-20);
                    //sb.SetHeight(sb.MaxWidth);
                    TableRow.LayoutParams param = new TableRow.LayoutParams(TableRow.LayoutParams.MatchParent, TableRow.LayoutParams.MatchParent);
                    //sb.LayoutParameters = new TableRow.LayoutParams(j);
                    param.SetMargins(5,5,5,5);
                    but.SoundEffectsEnabled = false;
                    but.LayoutParameters = param;

                    but.Click += sbOnClick;
                    but.LongClick += sbOnLongClick;
                    tr.AddView(but);

                }
                ImageButton mainMenu = FindViewById<ImageButton>(Resource.Id.mainMenuBt);
                mainMenu.Click += mainMenuClick;

            }


        }

        private void sbOnLongClick(object sender, View.LongClickEventArgs e)
        {
            SoundButton sb = sender as SoundButton;
            var buttonConfActivity = new Intent(this, typeof(ButtonConfActivity));
            buttonConfActivity.PutExtra("ID", sb.sId);
            StartActivity(buttonConfActivity);
            this.Finish();
        }

        private void mainMenuClick(object sender, EventArgs e)
        {
            SoundButton b = sender as SoundButton;
            try
            {
                foreach (MediaPlayer p in mpList)
                {
                    player = p;
                    player.Release();
                    player.Stop();
                    mpList.Remove(p);
                }
                
            }
            catch (Exception ex)
            {
                

            }
            
            this.Finish();
        }
        private void sbOnClick(object sender, EventArgs e)
        {
            SoundButton sb = sender as SoundButton;
            if (sb.sound == null)
            {
                //Liste der Files in Raw
                var fields = typeof(Resource.Raw).GetFields();
                //Name des
                string s = fields[sb.sId].Name;
                //int fileid = Resources.GetIdentifier(fields[sb.sId].Name, "Raw", PackageName);
                int fileid = (int)typeof(Resource.Raw).GetField(s).GetValue(null);
                player = MediaPlayer.Create(this, fileid);
            }
            else
            {
                player = new MediaPlayer();
                player.SetDataSource(sb.sound.Path);
                player.Prepare();
            }
            
            
            player.Start();
            mpList.Add(player);
            player.Completion += delegate { player.Release(); mpList.Remove(player); };

            
        }

        
        //private void ListRaw()
        //{
        //    var fields = typeof(Resource.Raw).GetFields();

        //    foreach (var fieldInfo in fields)
        //    {
        //        System.Diagnostics.Debug.WriteLineIf(fieldInfo.IsLiteral, fieldInfo.Name);
        //    }
        //}
    }
}